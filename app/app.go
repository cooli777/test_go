package app

import (
	"flag"
	"fmt"
	"github.com/go-redis/redis"
	"strconv"
	"time"
)

const (
	RedisAddressConnect = "localhost:6379"

	FieldIdApp       = "id"
	FieldGeneratorId = "generatorId"

	TypeAppGenerator = 1
	TypeAppHandler   = 2

	TimeGenerateMessage    = 500 * time.Millisecond
	TimeRenewId            = 1 * time.Second
	TimeHandleMessage      = 5 * time.Millisecond
	TimeLifeAppGeneratorId = 2 * time.Second
	PercentError           = 50
	AllPercent             = 100
	MessageQueue           = "message_queue"
	MessageQueueErrors     = "message_queue_errors"
)

type ExecuteInterface interface {
	Exec()
	getTypeApp() int
}

type App struct {
	id       int64
	client   *redis.Client
	typeApp  int
	logicApp map[int]ExecuteInterface
}

//Exec выбирает в логику как работает app и запускает работу
//выводит ошибки
//работает генератором
//работает обработчиком сообщений
func (self *App) Exec() {
	getErrors := flag.Bool("getErrors", false, "a bool")
	flag.Parse()

	if *getErrors == true {
		createAppHandlerErrors(self).Exec()
		return
	}

	for true {
		self.selectAndSetTypeApp()
		self.logicApp[self.typeApp].Exec()
	}
}

//создает App c дефолтными настройками
func CreateApp() *App {
	app := &App{client: createRedisClient(), logicApp: make(map[int]ExecuteInterface)}
	app.generateId()
	app.addLogicApp(createGenerator(app))
	app.addLogicApp(createAppHandler(app))
	return app
}

//addLogicApp добавить логику работы приложения
func (self *App) addLogicApp(logic ExecuteInterface) {
	self.logicApp[logic.getTypeApp()] = logic
}

//generateId получает id и присваивает его приложению
func (self *App) generateId() {
	self.id = self.client.Incr(FieldIdApp).Val()
}

//selectAndSetTypeApp определяем тип работы приложения
func (self *App) selectAndSetTypeApp() {

	idGenerator := self.getGeneratorIdFromRedis()

	if idGenerator == self.id || self.setAppIdToGeneratorId() {
		self.typeApp = TypeAppGenerator
	} else {
		self.typeApp = TypeAppHandler
	}
}

//getGeneratorIdFromRedis пробуем получить id генератора
func (self *App) getGeneratorIdFromRedis() int64 {
	idGeneratorRaw, err := self.client.Get(FieldGeneratorId).Result()

	if idGeneratorRaw == "" {
		return 0
	}

	if err != nil {
		fmt.Printf("error %+v", err)
		panic(err)
	}

	var idGenerator int64

	idGenerator, err = strconv.ParseInt(idGeneratorRaw, 10, 64)

	if err != nil {
		panic(err)
	}

	return idGenerator
}

//setAppIdToGeneratorId попытка устоновить id генератору id этого экземпляра апп
func (self *App) setAppIdToGeneratorId() bool {
	result, err := self.client.SetNX(FieldGeneratorId, self.id, TimeLifeAppGeneratorId).Result()

	if err != nil {
		panic(err)
	}

	return result
}

//createRedisClient создание клента редиса
func createRedisClient() *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     RedisAddressConnect,
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	_, err := client.Ping().Result()
	if err != nil {
		panic(err)
	}
	return client
}
