package app

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

//createGenerator создать генератор с начальными установками
func createGenerator(app *App) *Generator {
	return &Generator{app: app, channelStop1: make(chan string), channelStop2: make(chan string)}
}

type Generator struct {
	app          *App
	channelStop1 chan string
	channelStop2 chan string
}

//Exec создание сообщений и помещение их в очередь для обработки
//обновляет время ид генератора
//генерирует сообщение
func (self *Generator) Exec() {
	var wg sync.WaitGroup
	wg.Add(2)

	tickerRenewId := time.NewTicker(TimeRenewId)
	go func() {
		for {
			select {
			case <-tickerRenewId.C:
				self.renewGeneratorId()
			case <-self.channelStop1: // triggered when the stop channel is closed
				wg.Done()
				self.channelStop2 <- "stop"
				break
			}
		}
	}()

	tickerGenerateMessage := time.NewTicker(TimeGenerateMessage)
	go func() {
		for {
			select {
			case <-tickerGenerateMessage.C:
				self.pushMessage()
			case <-self.channelStop2: // triggered when the stop channel is closed
				wg.Done()
				break
			}
		}
	}()

	wg.Wait()
}

//getTypeApp получить тип getTypeApp
func (self *Generator) getTypeApp() int {
	return TypeAppGenerator
}

//pushMessage создать и отправить сообщение в очередь
func (self *Generator) pushMessage() {
	message := randStringRunes(10)
	self.app.client.RPush(MessageQueue, message)
	fmt.Printf("pushMessage: %s\n", message)
}

//renewGeneratorId обновить время id генератора, в случае неудачи завершить работу Exec
func (self *Generator) renewGeneratorId() {
	result, _ := self.app.client.SetXX(FieldGeneratorId, self.app.id, TimeLifeAppGeneratorId).Result()
	if !result {
		self.stopApp()
	}
}

//stopApp остоновить приложение
func (self *Generator) stopApp() {
	self.channelStop1 <- "stop"
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

//randStringRunes создание рандомной строки
func randStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
