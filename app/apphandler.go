package app

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

//createAppHandler создание структуры AppHandler вместе с начальными устоновками
func createAppHandler(app *App) *AppHandler {
	return &AppHandler{app: app, channelStop1: make(chan string), channelStop2: make(chan string)}
}

type AppHandler struct {
	app          *App
	channelStop1 chan string
	channelStop2 chan string
}

//Exec выполнение логики обработки сообщений
//по таймеру пробуют обработаться сообщения
//по таймеру проверяеться есть ли генератор
func (self *AppHandler) Exec() {
	var wg sync.WaitGroup
	wg.Add(2)

	tickerHandleMessage := time.NewTicker(TimeHandleMessage)
	go func() {
		for {
			select {
			case <-tickerHandleMessage.C:
				self.handleMessage()
			case <-self.channelStop1: // triggered when the stop channel is closed
				wg.Done()
				fmt.Println("stop 1")
				self.channelStop2 <- "stop"
				return
				break
			}
		}
	}()

	tickerUpdateGeratorId := time.NewTicker(TimeRenewId)
	go func() {
		for {
			select {
			case <-tickerUpdateGeratorId.C:
				self.checkAndSetGeneratorId()
			case <-self.channelStop2: // triggered when the stop channel is closed
				wg.Done()
				fmt.Println("stop 2")
				return
				break
			}
		}
	}()

	wg.Wait()
}

//getTypeApp получить тип getTypeApp
func (self *AppHandler) getTypeApp() int {
	return TypeAppHandler
}

//handleMessage обработка сообщений
func (self *AppHandler) handleMessage() {
	result := self.app.client.LPop(MessageQueue)
	if result.Val() != "" {
		if self.isErrorMessage() {
			self.app.client.LPush(MessageQueueErrors, result.Val())
			fmt.Printf("Error message %+v\n", result.Val())
		} else {
			fmt.Printf("Handled message %+v\n", result.Val())
		}
	}

}

//isErrorMessage содержит ли ошибку это сообщение
func (self *AppHandler) isErrorMessage() bool {
	errorPercent := rand.Intn(AllPercent+1-PercentError) + PercentError
	return errorPercent == AllPercent
}

//stopApp остановка app
func (self *AppHandler) stopApp() {
	self.channelStop1 <- "stop"
}

//checkAndSetGeneratorId попытка устоновить генератор Id, в случае успеха закончить выполнение метода Exec
func (self *AppHandler) checkAndSetGeneratorId() {
	result, _ := self.app.client.SetNX(FieldGeneratorId, self.app.id, TimeLifeAppGeneratorId).Result()
	if result {
		self.stopApp()
	}
}
