package app

import "fmt"

//createAppHandlerErrors создание AppHandlerErrors
func createAppHandlerErrors(app *App) *AppHandlerErrors {
	return &AppHandlerErrors{app: app}
}

type AppHandlerErrors struct {
	app *App
}

//Exec Извлечение всех сообщений с ошибками и удаление их из очереди
func (self *AppHandlerErrors) Exec() {
	for true {
		result := self.app.client.LPop(MessageQueueErrors)
		if result.Val() == "" {
			break
		}
		fmt.Printf("Handled error message: %+v\n", result.Val())
	}
	fmt.Printf("Handled error message complete\n")
}
