package main

import (
	"generator/app"
)

func main() {
	app.CreateApp().Exec()
}
